#!flask/bin/python
from flask import Flask, url_for
from flask import request

# python -m flask run --host=0.0.0.0


class Message:
    def __init__(self):
        pass


app = Flask(__name__)

# http://localhost:5000/message?user1=joao&user2=maria


@app.route('/get_message', methods=['GET'])
def get_message():
    user1 = request.args.get('user1')  # if key doesn't exist, returns None
    user2 = request.args.get('user2')

    s = "FROM: {0} TO: {1}".format(user1, user2)
    return s

@app.route('/post', methods=['POST'])
def post():
    data = request.form['key']
    return data


@app.route('/users')
def index():
    return "Hello, World!"

if __name__ == '__main__':
    app.run(debug=True)
